//
//  MasterViewController.swift
//  ModernDayGuide
//
//  Created by Mark Lopez on 6/5/16.
//  Copyright © 2016 BizarroApps. All rights reserved.
//

import UIKit

protocol LevelsSelectionDelegate: class {
    func levelsSelected(newLevels: Levels)
}


class MasterViewController: UITableViewController {
    weak var delegate: LevelsSelectionDelegate?
    var levels = [Levels]()
    var aboutUsArray = ["About this App", "Rate Us on the App Store", "Version 1.0.0"]
    

    

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        self.title = " "
        
        //==================================
        // MODIFY THE TITLEBAR BG COLOR
        //==================================
        
        //self.navigationController?.navigationBar.barTintColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
        self.tableView.backgroundColor = UIColor.darkGrayColor()
        self.navigationController?.navigationBarHidden = true

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        // LEVEL 1
        self.levels.append(Levels(
            name: "Modern Day Day 1",
            description: "Difficulty: Easy", details: "PRO TIP: Remember to use Primal Sunflower, Cold Snapdragon, Grapeshot, Primal Walnut, Peashooter, Ghostpepper, Iceberg Lettuce, and Cherry Bomb.",
            imageName: "level01",iconName: "circle01",
            audioLength: "01:30", trackName: "01"))

        // LEVEL 2
        self.levels.append(Levels(
            name: "Modern Day Day 2",
            description: "Difficulty: Easy", details: "PRO TIP: Remember to use Moonflower, Cold Snapdragon, Grapeshot, Primal Walnut, Shrinking Violet, Peashooter and Primal Potato Mine.",
            imageName: "level02",iconName: "circle02",
            audioLength: "01:20", trackName: "02"))

        // LEVEL 3
        self.levels.append(Levels(
            name: "Modern Day Day 3",
            description: "Difficulty: Easy", details: "PRO TIP: In this level there will be no portals and your plants will arrive on a conveyor belt. Use your Shrinking Violets to make the Newspaper Zombies and the Gargantuar Prime weaker.",
            imageName: "level03",iconName: "circle03",
            audioLength: "01:15", trackName: "03"))
        
        // LEVEL 4
        self.levels.append(Levels(
            name: "Modern Day Day 4",
            description: "Difficulty: Easy", details: "PRO TIP: In this level there will be no portals. There will be a lot of imps very early on. Remember to use Moonflower, Cold Snapdragon, Grapeshot, Primal Walnut, Shrinking Violet, Escape Root, Ghost Pepper, and Jalepeno.",
            imageName: "level04",iconName: "circle04",
            audioLength: "01:05", trackName: "04"))

        // LEVEL 5
        self.levels.append(Levels(
            name: "Modern Day Day 5",
            description: "Difficulty: Easy", details: "PRO TIP: Remember to use Nightshade, Moonflower, Cold Snapdragon, Grapeshot, Primal Walnut, Shrinking Violet, Primal Potato Mine, and Jalepeno.",
            imageName: "level05",iconName: "circle05",
            audioLength: "01:51", trackName: "05"))
       
        // LEVEL 6
        self.levels.append(Levels(
            name: "Modern Day Day 6",
            description: "Difficulty: Easy", details: "PRO TIP: There is a goal you must complete in this level. You must produce 5000 suns. There are Gold Tiles located very close to where the zombies appear.",
            imageName: "level06",iconName: "circle06",
            audioLength: "01:46", trackName: "06"))
        
        
        // LEVEL 7
        self.levels.append(Levels(
            name: "Modern Day Day 7",
            description: "Difficulty: Easy", details: "PRO TIP: In this level there will be a Dark Ages Portal. Use the Cold Snapdragons to slow down the zombies and use the Primal Walnuts to protect them.",
            imageName: "level07",iconName: "circle07",
            audioLength: "01:18", trackName: "07"))
        
        // LEVEL 8
        self.levels.append(Levels(
            name: "Modern Day Day 8",
            description: "Difficulty: Easy", details: "This is a Begouled level and you must make 100 matches to destroy the zombies and complete the level. I recommend you upgrade your Snapdragon to Cold Snapdragon and your Walnuts to Endurians.",
            imageName: "level08",iconName: "circle08",
            audioLength: "02:05", trackName: "08"))
        
        // LEVEL 9
        self.levels.append(Levels(
            name: "Modern Day Day 9",
            description: "Difficulty: Easy", details: "In this level, there is a goal for you to complete and you must complete the level with the given plants.",
            imageName: "level09",iconName: "circle09",
            audioLength: "02:04", trackName: "09"))
        
        // LEVEL 10
        self.levels.append(Levels(
            name: "Modern Day Day 10",
            description: "Difficulty: Easy", details: "PRO TIP: In this level there will be power tiles. Remember to use Nightshade, Moonflower, Cold Snapdragon, Grapeshot, Primal Walnut, Shrinking Violet, Primal Potato Mine, and Blover. Use the Blovers to defeat the Jetpack Zombies.",
            imageName: "level10",iconName: "circle10",
            audioLength: "01:35", trackName: "10"))
        
        // LEVEL 11
        self.levels.append(Levels(
            name: "Modern Day Day 11",
            description: "Difficulty: Easy", details: "PRO TIP: In this level, there are no portals. But you have to use the Shadowshroom, which you just unlocked.",
            imageName: "level11",iconName: "circle11",
            audioLength: "01:41", trackName: "11"))
        
        
        // LEVEL 12
        self.levels.append(Levels(
            name: "Modern Day Day 12",
            description: "Difficulty: Easy", details: "PRO TIP: In this level, there are no portals but there is a goal you have to complete. The player must not lose more than 10 plants. And this is the first level that has dinosaurs.",
            imageName: "level12",iconName: "circle12",
            audioLength: "01:54", trackName: "12"))
        
        // LEVEL 13
        self.levels.append(Levels(
            name: "Modern Day Day 13",
            description: "Difficulty: Easy", details: "PRO TIP: This is a Begouled level. When can, you should upgrade your Puff-shrooms to Fume-shrooms. And upgrade your Walnuts to Endurians.",
            imageName: "level13",iconName: "circle13",
            audioLength: "01:17", trackName: "13"))
        
        // LEVEL 14
        self.levels.append(Levels(
            name: "Modern Day Day 14",
            description: "Difficulty: Easy", details: "PRO TIP: In this level, you have to survive with the given plants. When the wave starts there will be some Jurassic Imps. You can defeat them with the Shrinking Violet. Remember to use your Shadow Shrooms against the Newspaper Zombies. And use the Shrinking Violet against the Gargantuar so he will be easier to defeat.",
            imageName: "level14",iconName: "circle14",
            audioLength: "01:27", trackName: "14"))
        
        // LEVEL 15
        self.levels.append(Levels(
            name: "Modern Day Day 15",
            description: "Difficulty: Easy", details: "PRO TIP: In this level, you have to protect 2 Primal Walnuts. I recommend using the Moonflowers for producing sun and to power up the shadow plants. And use the Blover to blow away the Balloon Zombies.",
            imageName: "level15",iconName: "circle15",
            audioLength: "01:20", trackName: "15"))
        
        // LEVEL 16
        self.levels.append(Levels(
            name: "Modern Day Day 16",
            description: "Difficulty: Easy", details: "PRO TIP: This is the Modern Day Gargantuar battle. Beware! I recommend you place your Winter Melons in column 1. Be sure to use the Intensive Carrot to heal your damaged plants, especially when the Gargantuar Prime uses his laser.",
            imageName: "level16",iconName: "circle16",
            audioLength: "01:30", trackName: "16"))
        
        // LEVEL 17
        self.levels.append(Levels(
            name: "Modern Day Day 17",
            description: "Difficulty: Easy", details: "PRO TIP: In this level, there is an Ancient Egypt portal. This is the first time you get to use the Dusk Lobber.",
            imageName: "level17",iconName: "circle17",
            audioLength: "03:18", trackName: "17"))
        
        // LEVEL 18
        self.levels.append(Levels(
            name: "Modern Day Day 18",
            description: "Difficulty: Easy", details: "PRO TIP: In this level, there are no portals. I recommend using your the moonflowers for powering up the Dusk Lobber and the other shadow plants. Dusk lobbers are good for damaging zombies in multiple lanes.",
            imageName: "level18",iconName: "circle18",
            audioLength: "02:01", trackName: "18"))
        
        // LEVEL 19
        self.levels.append(Levels(
            name: "Modern Day Day 19",
            description: "Difficulty: Easy", details: "PRO TIP: This is a conveyor belt level with a Big Wave Beach Portal. You get the Primal Peashooter, Laser Beans, Coconut Cannons, and and the premium plant for Modern Day Part 2: Escape Roots.",
            imageName: "level19",iconName: "circle19",
            audioLength: "01:51", trackName: "19"))
        
        // LEVEL 20
        self.levels.append(Levels(
            name: "Modern Day Day 20",
            description: "Difficulty: Easy", details: "PRO TIP: In this level, there are Gold Tiles, but no portals. You have to face, Basic Zombies, Conehead Zombies, Buckethead Zombies, Newspaper Zombies, Balloon Zombies, Punk Zombies and Jurassic Gargantuar",
            imageName: "level20",iconName: "circle20",
            audioLength: "02:33", trackName: "20"))
        
        // LEVEL 21
        self.levels.append(Levels(
            name: "Modern Day Day 21",
            description: "Difficulty: Easy", details: "PRO TIP: This level has a Ancient Egypt, Big Wave Beach Portal and a Neon Mixtape Portal. You will encounter the new All-star zombie in this level that moves very fast to tackle your plants.",
            imageName: "level21",iconName: "circle21",
            audioLength: "03:10", trackName: "21"))
        
        
        // LEVEL 22
        self.levels.append(Levels(
            name: "Modern Day Day 22",
            description: "Difficulty: Easy", details: "PRO TIP: This is another Begouled level, just like level 13 except now you have to make 75 matches.",
            imageName: "level22",iconName: "circle22",
            audioLength: "00:15", trackName: "22"))
        
        // LEVEL 23
        self.levels.append(Levels(
            name: "Modern Day Day 23",
            description: "Difficulty: Easy", details: "PRO TIP: In this level, the player has to use the new plant Grimrose. It can drag down a single zombie, kinda like a Tanglekelp. I recommend you use the Moonflower for powering up the Grimrose and the other shadow plants.",
            imageName: "level23",iconName: "circle23",
            audioLength: "03:45", trackName: "23"))
        
        // LEVEL 24
        self.levels.append(Levels(
            name: "Modern Day Day 24",
            description: "Difficulty: Easy", details: "PRO TIP: Watch out for the Superfan Imps in this level. It can be punted by the All-star zombie while doing his tackle move. So I recommend you use the Moonflowers for powering up your shadow plants. And use the Blovers to blow away the balloon zombies.",
            imageName: "level24",iconName: "circle24",
            audioLength: "03:13", trackName: "24"))
        
        // LEVEL 25
        self.levels.append(Levels(
            name: "Modern Day Day 25",
            description: "Difficulty: Easy", details: "PRO TIP: This is a conveyor belt level. You  get Intensive Carrots and you get introduced to Modern Tiles that behave like Ice Blows back in Frostbite Caves. Try to keep the Kernel Pults revived so they will slow the zombies down with butter. Use the Fume-shrooms against the All-star Zombies.",
            imageName: "level25",iconName: "circle25",
            audioLength: "02:52", trackName: "25"))
        
        // LEVEL 26
        self.levels.append(Levels(
            name: "Modern Day Day 26",
            description: "Difficulty: Easy", details: "PRO TIP: This level has a Raptor dinosaur. I recommend you use the Moonflower to power up those Shadow Plants. Use the Dusk Lobber to cause damage in multiple lanes. Defeat the Disco Jet Zombies with the Blover.",
            imageName: "level26",iconName: "circle26",
            audioLength: "02:46", trackName: "26"))
        
        
        // LEVEL 27
        self.levels.append(Levels(
            name: "Modern Day Day 27",
            description: "Difficulty: Easy", details: "PRO TIP: This level is the same as level 8 Begouled Level.",
            imageName: "level27",iconName: "circle27",
            audioLength: "00:08", trackName: "27"))
        
        // LEVEL 28
        self.levels.append(Levels(
            name: "Modern Day Day 28",
            description: "Difficulty: Easy", details: "PRO TIP: The player has a goal in this level and must produce at least 3000 suns in this level. Moonflowers are the highest sun producing plants, so I recommend you use these. Use the Shadowshrooms to poson the Newspaper Zombies.",
            imageName: "level28",iconName: "circle28",
            audioLength: "02:21", trackName: "28"))
        
        // LEVEL 29
        self.levels.append(Levels(
            name: "Modern Day Day 29",
            description: "Difficulty: Easy", details: "PRO TIP: There are Tombstones on the lawn for this level. I recommend you use the Moonflowers for sun production and powering up those Shadow plants. Use the Gravebuster and Grapeshot to remove those graves. Use the Cold Snapdragon to slow down those Breakdancer Zombies.",
            imageName: "level29",iconName: "circle29",
            audioLength: "02:46", trackName: "29"))
        
        // LEVEL 30
        self.levels.append(Levels(
            name: "Modern Day Day 30",
            description: "Difficulty: Easy", details: "PRO TIP: You have a very difficult goal to complete in this level: You must not let the zombies trample the flowers. Use the Cold Snapdragon to slow down and put put out the Prospector Zombies fuse. Use the Primal Walnuts as a good defense.",
            imageName: "level30",iconName: "circle30",
            audioLength: "03:01", trackName: "30"))
        
        // LEVEL 31
        self.levels.append(Levels(
            name: "Modern Day Day 31",
            description: "audioLength: Easy", details: "PRO TIP: There are 3 Modern Day slider tiles and you have to use the Escape Root. Use the Cold Snapdragon to slow down and damage the zombies.",
            imageName: "level31",iconName: "circle31",
            audioLength: "03:02", trackName: "31"))
        
        
        // LEVEL 32
        self.levels.append(Levels(
            name: "Modern Day Day 32",
            description: "Difficulty: Easy", details: "PRO TIP: In this level, the player has unlocked the first Dr. Zomboss level. You will encounter either the Zombot Sphinx-inator, the Zombot PlankWalk, or the Zombot War Wagon.",
            imageName: "level32",iconName: "circle32",
            audioLength: "05:22", trackName: "32"))
        
        // LEVEL 33
        self.levels.append(Levels(
            name: "Modern Day Day 33",
            description: "Difficulty: Easy", details: "PRO TIP: In this level, the player has encountered the second Dr. Zomboss Battle. You will encounter either the Zombot Tomorrow-tron, the Zombot Dark Dragon, or the Zombot Sharktronic Sub.",
            imageName: "level33",iconName: "circle33",
            audioLength: "04:44", trackName: "33"))
        
        // LEVEL 34
        self.levels.append(Levels(
            name: "Modern Day Day 34",
            description: "Difficulty: Easy", details: "PRO TIP: This is the final Modern Day Dr. Zomboss Battle. The player will encounter either the Zombot Tuskmaster 10,000BC, the Zombot Aerostatic Gondola, the Zombot Multi-stage Masher or the Zombot Dinotronic Mehasaur.",
            imageName: "level34",iconName: "circle34",
            audioLength: "08:21", trackName: "34"))
        
        
        // About this app button
        self.levels.append(Levels(
            name: "About Us",
            description: "", details: "",
            imageName: "",iconName: "icon_aboutUs2",
            audioLength: "Details About this App", trackName: ""))
        
        // Rate Us on the App Store
        self.levels.append(Levels(
            name: "Rate Us",
            description: "", details: "",
            imageName: "",iconName: "icon_rateUs2",
            audioLength: "Give Us Your Feedback", trackName: ""))
        
        // Version 1.0.0
        self.levels.append(Levels(
            name: "Version 1.0.0",
            description: "", details: "",
            imageName: "",iconName: "icon_appVersion",
            audioLength: "", trackName: ""))
        
    }
    
    
    
    
    override func tableView(tableView: UITableView, willSelectRowAtIndexPath indexPath: NSIndexPath) -> NSIndexPath? {
        //if we are in the second section, then determine what to do when these buttons are tapped
        if (indexPath.section == 1) {
            self.triageOtherRows(indexPath.row)
            return nil
        }
        return indexPath
    }
    
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        let selectedLevels = self.levels[indexPath.row]
        self.delegate?.levelsSelected(selectedLevels)
   
        //this makes the table row selection 'work' on iPhone as well as iPad
        if let detailViewController = self.delegate as? DetailViewController {
            splitViewController?.showDetailViewController(detailViewController.navigationController!, sender: nil)
        }
    }// close didSelectRowAtIndexPath
    
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }


    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section){
        case 0:
            return self.levels.count-3
        case 1:
            return 3
        default:
            return 1
        }
    }

    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "tblviewCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! LevelsCell
        
        if (indexPath.section == 1) {
            
            if(indexPath.row == 0){
                let level = self.levels[34]
                cell.thumbnail.image = UIImage(named: level.iconName)
                cell.titleLabel.text = level.name
                cell.subtitleLabel.text = level.audioLength
            }
            else if(indexPath.row == 1){
                let level = self.levels[35]
                cell.thumbnail.image = UIImage(named: level.iconName)
                cell.titleLabel.text = level.name
                cell.subtitleLabel.text = level.audioLength
            }
            else if(indexPath.row == 2){
                let level = self.levels[36]
                cell.thumbnail.image = UIImage(named: level.iconName)
                cell.titleLabel.text = level.name
                cell.subtitleLabel.text = level.audioLength
            }
            //leave this return here
            return cell

        }
            
        else{
            let level = self.levels[indexPath.row]
            cell.thumbnail.image = UIImage(named: level.iconName)
            cell.titleLabel.text = level.name
            cell.subtitleLabel.text = level.audioLength
            return cell
        }
    }
    
    
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        //Let's adjust the table cell bg color
        switch (indexPath.section){
        case 0:
           cell.backgroundColor = UIColor(red: 79/255.0, green: 75/255.0, blue: 79/255.0, alpha: 1.0)
            break
        case 1:
            cell.backgroundColor = UIColor(red: 124/255.0, green: 112/255.0, blue: 139/255.0, alpha: 1.0)
            
            break
        default:
            break
        }
    }
    
    
/*
 I decided not to use this for v1 becs this is a poor implementation.
 the proper way to execute my goal of having section 2 look different from section 1
 table cells would be to have a completely different custom table cell class

    
    
    func createDynLabel(rowNum:Int)-> UILabel{
        //==================================
        // CREATE A NEW LABEL FOR MY 'DIFFERENT' CELL LABELS
        //==================================
        let aboutLabel = UILabel(frame: CGRectMake(35, 38, 300, 30))
        aboutLabel.textAlignment = NSTextAlignment.Left
        
        // CREATE SOME CUSTOM FONT TREATMENTS
        
        let aboutFont: NSDictionary = [
            NSFontAttributeName: UIFont(name: "HelveticaNeue-Bold", size: 20.0)!,
            NSForegroundColorAttributeName: UIColor.whiteColor()
        ]
        
        let aboutAttrString = NSAttributedString(string: self.aboutUsArray[rowNum], attributes: (aboutFont as! [String : AnyObject]))
        
        aboutLabel.attributedText = aboutAttrString
        return aboutLabel
    }
 
 */
    
    
    
    
    
    func triageOtherRows(rowNum:Int){
        switch (rowNum) {
            case 0:
            self.handleAboutUs()
            break
            
        case 1:
            self.handleRateUs()
            break
            
        case 2:
            //just the version number. do nothing
            break
        default:
            break
        }
    }
    
    
    
    func handleAboutUs(){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let AboutVC = storyBoard.instantiateViewControllerWithIdentifier("AboutVC")
        self.presentViewController(AboutVC, animated: false, completion: nil)
    }
    
    
    func handleRateUs(){
        UIApplication.sharedApplication().openURL(NSURL(string: "http://appstore.com/pvz2moderndayaudioguide")!)
    }
    
    

}
