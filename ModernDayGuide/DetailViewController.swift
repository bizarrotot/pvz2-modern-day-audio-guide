//
//  DetailViewController.swift
//  ModernDayGuide
//
//  Created by Mark Lopez on 6/5/16.
//  Copyright © 2016 BizarroApps. All rights reserved.
//



/*
 
 1. create
 @property (nonatomic, strong) AVAudioPlayer *audioPlayer;
 outside of the viewdidload
 
 
 2.
 the audio player should be created upon viewdidload
 
 self.audioPlayer = [[AVAudioPlayer alloc] initWithData:fileData
 error:&error];
 [self.audioPlayer prepareToPlay];
 
 
 3.
 the play button should have a simple
 [self.audioPlayer play];
 
 4.
 the pause button should have a simple
 [self.audioPlayer pause];
 
 
 
 
 step 1 fix
 properly create an audio player
 hardcode the resource
 
 make it play and pause correctly
 
 
 */


import UIKit
import AVFoundation

class DetailViewController: UIViewController {
    // MARK: Properties
    @IBOutlet weak var levelGraphic: UIImageView!
    @IBOutlet weak var levelTitle: UILabel!
    @IBOutlet weak var levelSubtitle: UILabel!
    @IBOutlet weak var levelDifficulty: UILabel!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var controlBackground: UIView!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var detailsTextView: UITextView!
    @IBOutlet weak var playedTime: UILabel!
    @IBOutlet weak var totalTime: UILabel!
    
    var audioPlayer = AVAudioPlayer()
    var audioPath:String!
    var isPlaying = false
    var isPaused = false
    var timer:NSTimer!
    
    
    
    
    
    var levels: Levels! {
        didSet (newLevels) {
            self.refreshUI()
        }
    }
    

  
    
    
    
    
    func setupAudioPlayerWithFile(file:NSString, type:NSString) -> AVAudioPlayer?  {
        
        let path = NSBundle.mainBundle().URLForResource(file as String, withExtension: type as String)
        


        
        do{
            try audioPlayer = AVAudioPlayer(contentsOfURL: path!)
            

        }
        
        catch let error as NSError {
            print(error.description)
               print("Player not available")
        }
        
        /*
        //1
        let path = NSBundle.mainBundle().pathForResource(file as String, ofType: type as String)
        let url = NSURL.fileURLWithPath(path!)
        
        //2
        var audioPlayer:AVAudioPlayer?
        
        // 3
        do {
            try audioPlayer = AVAudioPlayer(contentsOfURL: url)
        } catch {
            print("Player not available")
        }
         */
        return audioPlayer
     
    }
 
 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    
        progressView.progress = 0.0
        progressView.progressTintColor = UIColor(red: 245/255.0, green: 162/255.0, blue: 0/255.0, alpha: 1.0)
        progressView.trackTintColor = UIColor(red: 187/255.0, green: 80/255.0, blue: 2/255.0, alpha: 1.0)
        //Modify the height of the progress bar
        progressView.transform = CGAffineTransformScale(progressView.transform, 1, 13)
        
        controlBackground.backgroundColor = UIColor(patternImage: UIImage(named: "greenGradient.png")!)
        
        //==================================
        // MODIFY THE TITLEBAR BG COLOR
        //==================================
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 1.0)
        
        //==================================
        // MODIFY THE TITLEBAR TEXT
        //==================================
        
        let multipleAttributes: NSDictionary = [
            NSFontAttributeName: UIFont(name: "Marker Felt", size: 31.0)!,
            NSForegroundColorAttributeName: UIColor.whiteColor()
        ]
        
        let myTitleAttrString = NSAttributedString(string: "Plants vs. Zombies 2 Modern Day Audio Guide", attributes: (multipleAttributes as! [String : AnyObject]))
        
        
        let vcTitleLabel = UILabel(frame: CGRectMake(0, 0, 220, 30))
        vcTitleLabel.attributedText = myTitleAttrString
        vcTitleLabel.textAlignment = NSTextAlignment.Center
        vcTitleLabel.attributedText = myTitleAttrString
        
        self.navigationItem.titleView = vcTitleLabel
        
        //self.refreshUI()
        
        
        

        
        /*
 the audio path is not getting updated when a new level photo is loaded
         how about when you load the new photo, print out the value of audioPath?
 */
        
        
        
//        self.setupAudioPlayerWithFile(audioPath, type: "mp3")
        print("view did load fired")

    
    
}
    
    
    
    
    func refreshUI() {
        levelGraphic?.image = UIImage(named: levels.imageName)
        levelTitle?.text = levels.name
        levelSubtitle?.text = levels.description
        levelDifficulty?.text = levels.audioLength
        audioPath = levels.trackName
        
        //give some padding to make it look pretty
        detailsTextView?.textContainerInset = UIEdgeInsetsMake(15.0, 20.0, 10.0, 15.0) // top, left, bottom, right
        
        let myAttribute = [ NSFontAttributeName: UIFont(name: "Avenir Next", size: 21.0)! ]
        let myAttrString = NSAttributedString(string: levels.details, attributes: myAttribute)
        detailsTextView?.attributedText = myAttrString
        
        
        self.setupAudioPlayerWithFile(audioPath, type: "mp3")

    }
    
    
    // MARK: - playAudio
    @IBAction func playAudio(sender: UIButton) {
        self.audioPlayer.prepareToPlay()
        self.audioPlayer.play()
        
        //WE MUST ALWAYS SET THIS FLAG TO ALLOW US TO TRACK THE STATUS OF THE PLAYER
        isPlaying = true

        
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(DetailViewController.updateTime), userInfo: nil, repeats: true)
        progressView.setProgress(Float(audioPlayer.currentTime/audioPlayer.duration), animated: false)
        
        
    }//close playAudio
    
    
    
    
    // MARK: - stopAudio
    @IBAction func stopAudio(sender: AnyObject) {
        self.audioPlayer.stop()
        self.audioPlayer.currentTime = 0
        progressView.setProgress(0.0, animated: false)
        isPlaying = false
    }
    
    
    
    // MARK: - pauseAudio
    @IBAction func pauseAudio(sender: AnyObject) {
        self.audioPlayer.pause()
        isPlaying = false
    }
    

    
    func updateTime() {
        if isPlaying
        {
            //This controls the display of the timer on the LEFT
            let currentTime = Int(audioPlayer.currentTime)
            let minutes = currentTime/60
            let seconds = currentTime - minutes * 60
            playedTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
            
            //This controls the display of the timer on the RIGHT
            let audioTotal = Int(audioPlayer.duration)
            let countDown = audioTotal - currentTime
            let cdMinutes = countDown/60
            let cdSeconds = countDown-cdMinutes * 60
            totalTime.text = NSString(format: "%02d:%02d", cdMinutes, cdSeconds) as String
            
            // Update progress
            progressView.setProgress(Float(audioPlayer.currentTime/audioPlayer.duration), animated: true)
        }
    }
    

    
    
    // MARK: - didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
} // close the DetailViewController class



extension DetailViewController: LevelsSelectionDelegate {
    func levelsSelected(newLevels: Levels) {
        levels = newLevels
    }
}
