//
//  AboutVC.swift
//  ModernDayGuide
//
//  Created by Mark Lopez on 9/16/16.
//  Copyright © 2016 BizarroApps. All rights reserved.
//

import UIKit

class AboutVC: UIViewController {

    @IBOutlet weak var aboutDetailsScrollView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func closeBtn(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
        self.aboutDetailsScrollView.setContentOffset(CGPointMake(0, 0), animated: false)

    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
