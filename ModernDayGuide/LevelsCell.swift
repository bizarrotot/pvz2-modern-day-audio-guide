//
//  LevelsCell.swift
//  ModernDayGuide
//
//  Created by Mark Lopez on 9/1/16.
//  Copyright © 2016 BizarroApps. All rights reserved.
//

import UIKit

class LevelsCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var subtitleLabel: UILabel!

    
    var levels: Levels! {
        didSet (newLevels) {
            self.refreshUI()
        }
    }
    
    
    func refreshUI() {
        thumbnail?.image = UIImage(named: levels.imageName)
        titleLabel?.text = levels.name
        subtitleLabel?.text = levels.audioLength        
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    


}
