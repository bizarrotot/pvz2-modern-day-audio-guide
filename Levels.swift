//
//  Levels.swift
//  ModernDayGuide
//
//  Created by Mark Lopez on 6/5/16.
//  Copyright © 2016 BizarroApps. All rights reserved.
//

import UIKit

class Levels {
    let name: String
    let description: String
    let details: String
    let imageName: String
    let iconName: String
    let audioLength: String
    let trackName:String
    
    // initializer
    init(name: String, description: String, details:String, imageName: String, iconName: String, audioLength:String, trackName:String) {
        self.name = name
        self.description = description
        self.details = details
        self.imageName = imageName
        self.iconName = iconName
        self.audioLength = audioLength
        self.trackName = trackName
    }
}