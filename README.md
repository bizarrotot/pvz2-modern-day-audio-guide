# README #

### What is this repository for? ###

* Quick summary
This repository is for this IOS app called "Audio Walkthrough for PVZ2 Modern Day". The application is built in SWIFT using XCODE 7.3.1. This app was created around Oct. 2016 and deployed to the U.S. App Store in Nov. 2016. 

The app is meant to serve as a gameplay walkthrough specifically for Plants vs. Zombies 2 Modern Day World.

Deployment Target is IOS 9.0 for iPad.


* Version
V 1.0


### How do I get set up? ###

* Configuration
* Dependencies
Xcode 7.3 and Swift 2.2

* Database configuration
This app has not database hooks.

### Who do I talk to? ###

* Repo owner or admin
Mark Lopez
mailbox290@gmail.com